import React from 'react';
import styles from './page.module.css';
import Logo from '../logo/Logo';
import Navbar from '../nav/Navbar';

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.navItems}>
        <Logo />
        <Navbar />
      </div>
    </header>
  );
};

export default Header;
