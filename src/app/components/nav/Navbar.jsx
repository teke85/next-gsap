import React from 'react';
import Link from 'next/link';
import styles from './page.module.css';

const Navbar = () => {
  return (
    <section>
      <div className={styles.toggleMenu}>
        <ul className={styles.menuItems}>
          <li>
            <Link className="Link" href="/">
              Home
            </Link>
          </li>
          <li>
            <Link className="Link" href="/about">
              About
            </Link>
            <Link className="Link" href="/projects">
              Projects
            </Link>
            <Link className="Link" href="/contact">
              Contact
            </Link>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default Navbar;
